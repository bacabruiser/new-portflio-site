<!DOCTYPE html>
<html>
<head>
<!-- CSS
---------------------------------->
<link rel="stylesheet" href="foundation/css/foundation.css" />
<link rel="stylesheet" href="scss.php/style.scss" />

<!-- JS
---------------------------------->
<script src="foundation/js/vendor/modernizr.js"></script>

<!-- Meta
---------------------------------->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

<title>Index</title>
</head>
<body id="top-of-page">
<div id="intro" class="small-12-columns">
	<div id="logo">
		<img src="imgs/logo.png"/>
	</div>
	<div id="rocket-contain">
		<div id="rocket">
			<img src="imgs/rocketShip.png"/>
		</div>
		<div id="comet">
			<img src="imgs/comet.png"/>
		</div>
	</div>
</div>

<div id="site-container">
	<header>
        <div class="contain-to-grid">
            <nav class="top-bar" data-topbar>
                <ul class="title-area">
                    <li class="name">
                      <h3><a href="#top-of-page">DMACC Launch</a></h3>
										</li>
                    <li class="toggle-topbar menu-icon">
                      <a href="#">Menu</a>
                    </li>
                </ul>
                <section class="top-bar-section">
                    <ul class="right">
                        <li class="web-dev-nav"><a href="#web-dev">Web Development</a></li>
                        <li class="graphic-design-nav"><a href="#graphic-design">Graphic Design</a></li>
                        <li class="video-production-nav"><a href="#video">Video Production</a></li>
                        <li class="animation-nav"><a href="#animation">Animation</a></li>
                        <li class="photography-nav"><a href="#photography">Photography</a></li>
                    </ul>
                </section>
            </nav>
        </div>
    </header>
		
	<section id="top">
		<div id="astronaut">
			<img src="imgs/spaceman.png"/>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="row text-center center">
						<div class="small-12 columns planet-1">
							<img src="imgs/2.png" />
						</div>
						<div id="countdown">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="departments">
		<!-- START OF Web Dev GALLERY -->
		<section id="web-dev">
			<div class="row">
				<div class="medium-12-columns">
					<img class="web-dev-planet" src="imgs/web_dev.png" />
					<div id="web-dev-gallery" class="img-gallery">
						<div id="web-dev-students">
							<div class="text-center">
								<div class="large-3 columns">
									<div class="th">
										<img alt="SpongeBob" src="https://upload.wikimedia.org/wikipedia/en/5/5c/Spongebob-squarepants.png"/>
									</div>
								</div>
								<div class="large-3 columns">
									<div class="th">
										<img alt="Patrick" src="https://upload.wikimedia.org/wikipedia/en/7/7e/Patrick_Star.png"/>
									</div>
								</div>
								<div class="large-3 columns">
									<div class="th">
										<img alt="Squidward" src="http://cs418931.vk.me/v418931429/b28/FhIGLEbVP5c.jpg"/>
									</div>
								</div>
								<div class="large-3 columns end">
									<div class="th">
										<img alt="Mr.Krabs" src="http://www.ytv.com/sites/default/files/Mr_Krabs_And_Money.png"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- START OF Graphic Design GALLERY -->
		<section id="graphic-design">
			<div class="row text-center">
				<div class="medium-12-columns">
					<img class="graphic-design-planet" src="imgs/graphic_design.png" />
					<div id="graphic-design-gallery" class="img-gallery">
						<div id="graphic-design-students">
							<div class="large-3 columns">
								<div class="th">
									<img alt="Homer Simpson" src="http://madbetty.com/wp-content/uploads/2013/11/homer-simpson.jpg"/>
								</div>
							</div>
							<div class="large-3 columns">
								<div class="th">
									<img alt="Patrick" src="https://upload.wikimedia.org/wikipedia/en/7/7e/Patrick_Star.png"/>
								</div>
							</div>
							<div class="large-3 columns">
								<div class="th">
									<img alt="Squidward" src="http://cs418931.vk.me/v418931429/b28/FhIGLEbVP5c.jpg"/>
								</div>
							</div>
							<div class="large-3 columns end">
								<div class="th">
									<img alt="Mr.Krabs" src="http://www.ytv.com/sites/default/files/Mr_Krabs_And_Money.png"/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
	</section>
<!--
	<section id="bottom">
	</section>
	-->
	<link href="http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css" rel="stylesheet">
<footer id="footer" class="footer">
  <div class="row">
    <div class="small-12 medium-6 large-5 columns">
      <p class="logo"><i class="fi-shield"></i> DMACC</p>
      <p class="footer-links">
        <a href="#">Web Development</a>
        <a href="#">Graphic Design</a>
        <a href="#">Video Production</a>
        <a href="#">Animation</a>
        <a href="#">Photography</a>
        <p class="about">Portfolio Day
            Thursday April 28th, 2016
        <p class="about">11AM - 5PM
            Admission: FREE</p>
        <p class="copywrite">Copyright © 2016</p>
    </div>
    <div class="small-12 medium-6 large-4 columns">
      <ul class="contact">
        <li><p><i class="fi-marker">
            </i><p>Greater Des Moines Botanical Garden</p>
                <p>909 Robert D. Ray Dr,</p>
                <p>Des Moines, IA 50309</p>
            <li><p><i class="fi-telephone"></i>(515) 964-6200</p></li>
            <li><p><i class="fi-mail"></i>contact@dmacc.edu</p></li>
      </ul>
    </div>
    <div class="small-12 medium-12 large-3 columns">
      <p class="about">DMACC Portfolio Day</p>
      <p class="about subheader">Where students come togther to showcase thier talents, show off personal projects, and woo you with thier abilities.</p>
      <ul class="inline-list social">
        <a href="#"><i class="fi-social-facebook"></i></a>
        <a href="#"><i class="fi-social-linkedin"></i></a>
      </ul>
    </div>
  </div>
</footer>
	
	<!-- Overlay for images / profile -->
	<div id="overlay"></div>
	<div id="profile">
		<div class="outer">
				<div class="middle">
					<div class="inner">
						<div class="row text-center">
							<div class="large-6 columns">
								<div id="large-img"></div>
							</div>
							<div class="large-6 columns">
								<div id="bio-C:\wamp\www\portfolio_day">
									<h1 class="bio-title">Greetings!</h1>
									<div id="bio">
										
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
	
<!-- JS ---------------------------------->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="foundation/js/foundation.min.js"></script>
	<script src="js/script.js"></script>
	<script>
		$(document).foundation();
	</script>
</div>
</body>
</html>